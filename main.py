import cv2
import numpy as np
import utils

# var
# path = 'template/mcq-template.png'
path = 'template/mcq-filled.png'
# widthImg = 700
# heightImg = 700

img = cv2.imread(path)

# Preprocessing
# img = cv2.resize(img, (widthImg, heightImg))

# Get the size of the image
# print(img.shape)
widthImg = img.shape[0]
heightImg = img.shape[1]

imgContours = img.copy()
imgBiggestContours = img.copy()
imgGray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
imgBlue = cv2.GaussianBlur(imgGray, (5, 5), 1)
imgCanny = cv2.Canny(imgBlue, 10, 50)
imgBlank = np.zeros_like(img)

# finding all contours
contours, hierarchy = cv2.findContours(imgCanny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
cv2.drawContours(imgContours, contours, -1, (0, 255, 0), 10)

# find rectangles
rectangularContours = utils.rectangularContours(contours)
biggestContour = utils.getCornerPoints(rectangularContours[0])
secondContour = utils.getCornerPoints(rectangularContours[1])
thirdContour = utils.getCornerPoints(rectangularContours[2])
# print(biggestContour)
# print(len(biggestContour))

# potting contour in the imgBiggestContours
if biggestContour.size != 0 and secondContour.size != 0 and thirdContour.size != 0:
    cv2.drawContours(imgBiggestContours, biggestContour, -1, (0, 255, 0), 10)  # green
    cv2.drawContours(imgBiggestContours, secondContour, -1, (255, 0, 0), 10)  # red
    cv2.drawContours(imgBiggestContours, thirdContour, -1, (0, 0, 255), 10)  # blue

    biggestContour = utils.reOrder(biggestContour)
    secondContour = utils.reOrder(secondContour)
    thirdContour = utils.reOrder(thirdContour)

    pt1Biggest = np.float32(biggestContour)
    pt2Biggest = np.float32([[0, 0], [widthImg, 0], [0, heightImg], [widthImg, heightImg]])
    matrixBiggest = cv2.getPerspectiveTransform(pt1Biggest, pt2Biggest)
    imgWarpColoredBiggest = cv2.warpPerspective(img, matrixBiggest, (widthImg, heightImg))

    pt1Second = np.float32(secondContour)
    pt2Second = np.float32([[0, 0], [widthImg, 0], [0, heightImg], [widthImg, heightImg]])
    matrixSecond = cv2.getPerspectiveTransform(pt1Second, pt2Second)
    imgWarpColoredSecond = cv2.warpPerspective(img, matrixSecond, (widthImg, heightImg))

    pt1Third = np.float32(thirdContour)
    pt2Third = np.float32([[0, 0], [widthImg, 0], [0, heightImg], [widthImg, heightImg]])
    matrixThird = cv2.getPerspectiveTransform(pt1Third, pt2Third)
    imgWarpColoredThird = cv2.warpPerspective(img, matrixThird, (widthImg, heightImg))

    # Apply Threshold for biggest
    imgWarpGrayBiggest = cv2.cvtColor(imgWarpColoredBiggest, cv2.COLOR_RGB2GRAY)
    imgThresholdBiggest = cv2.threshold(imgWarpGrayBiggest, 170, 255, cv2.THRESH_BINARY_INV)[1]
    # print(len(imgThresholdBiggest))
    utils.splitBoxesMCQ(imgThresholdBiggest)  # MCQ for Biggest

    # Apply Threshold for second
    imgWarpGraySecond = cv2.cvtColor(imgWarpColoredSecond, cv2.COLOR_RGB2GRAY)
    imgThresholdSecond = cv2.threshold(imgWarpGraySecond, 170, 255, cv2.THRESH_BINARY_INV)[1]
    # print(len(imgThresholdSecond))
    # utils.splitBoxesMCQ(imgThresholdBiggest)  # MCQ for Biggest

    # Apply Threshold for biggest StudentID
    imgWarpGrayThird = cv2.cvtColor(imgWarpColoredThird, cv2.COLOR_RGB2GRAY)
    imgThresholdThird = cv2.threshold(imgWarpGrayThird, 170, 255, cv2.THRESH_BINARY_INV)[1]
    # print(len(imgThresholdThird))
    utils.splitBoxesMCQ(imgWarpGrayThird, boxType="studentID")  # MCQ for testing with mcq split box
    # utils.splitBoxesMCQ(imgThresholdThird)  # MCQ for second

# imageArray = ([img, imgGray, imgBlue, imgCanny, imgContours, imgBlank, imgBlank])

# Over All
imageArray = ([img, imgGray, imgBlue, imgCanny, imgContours, imgBiggestContours, imgWarpColoredBiggest, imgWarpColoredSecond, imgWarpColoredThird])

# Biggest
imageArray2 = ([imgBiggestContours, imgWarpGrayBiggest, imgThresholdBiggest])

imageStacked = utils.stackImages(0.5, imageArray)
imageStacked2 = utils.stackImages(0.5, imageArray2)

# cv2.imshow("Original", imageStacked)
# cv2.imshow("Biggest", imageStacked2)
cv2.waitKey(0)
