import cv2
import numpy as np


def stackImages(scale, imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range(0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape[:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]),
                                                None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y] = cv2.cvtColor(imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank] * rows
        hor_con = [imageBlank] * rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None, scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor = np.hstack(imgArray)
        ver = hor
    return ver


def rectangularContours(contours):
    rectContours = []
    for i in contours:
        area = cv2.contourArea(i)
        # print(area)
        if area > 50:
            peri = cv2.arcLength(i, True)
            approx = cv2.approxPolyDP(i, 0.02 * peri, True)
            # print(area, len(approx))
            if len(approx) == 4:
                # print(area, len(approx))
                rectContours.append(i)

    rectContours = sorted(rectContours, key=cv2.contourArea, reverse=True)

    return rectContours


def getCornerPoints(contour):
    peri = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.02 * peri, True)
    return approx


def reOrder(myPoints):
    myPoints = myPoints.reshape(4, 2)
    myPointsNew = np.zeros((4, 1, 2), np.int32)  # NEW MATRIX WITH ARRANGED POINTS
    add = myPoints.sum(1)
    myPointsNew[0] = myPoints[np.argmin(add)]  # [0,0]
    myPointsNew[3] = myPoints[np.argmax(add)]  # [w,h]
    diff = np.diff(myPoints, axis=1)
    myPointsNew[1] = myPoints[np.argmin(diff)]  # [w,0]
    myPointsNew[2] = myPoints[np.argmax(diff)]  # [h,0]
    return myPointsNew


def splitBoxesMCQ(img, boxType="MCQbox"):
    # if studentID box
    # if MCQbox box
    for i in range(2):
        img = np.vstack((img, img[0]))
    for i in range(2):
        img = np.vstack((img[0], img))

    boxes = []
    if boxType == 'MCQbox':
        i = 0
        rows = np.vsplit(img, 10)
        for r in rows:
            # i += 1
            print(r.shape)
            # cv2.imshow("splits: {}".format(i), r)
            # print(r)
            # print(type(r))
            cols = np.hsplit(r, np.array([10, 650]))
            print(len(cols))
            for box in cols:
                # boxes.append(box)
                cv2.imshow("boxes!!", box)
            print(cols)

        # print(r[1])
        # cols = np.hsplit(r, 10)
        # print(cols)
        # for box in cols:
        #     boxes.append(box)
        #     cv2.imshow("splits ", box)

    elif boxType == 'studentID':
        rows = np.vsplit(img, 5)
        # i = 0
        # for r in rows:
        #     print(len(r))
            # i += 1;
            # cv2.imshow("splits: {}".format(i), r)

            # cols = np.hsplit(r, 2)
        # for i in range(5):
        #     cv2.imshow("splits: {}".format(i), rows[i])
